package be.tbrx.google.hashcode.output;

import be.tbrx.google.hashcode.solver.Ingredient;
import be.tbrx.google.hashcode.solver.Output;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

public class OutputWriterTest {

    @Test
    void writeOutput() throws IOException {
        SampleOutput output = new SampleOutput("test_output.out.txt", new Output(Set.of(
                ingredient("cheese"),
                ingredient("mushrooms"),
                ingredient("tomatoes"),
                ingredient("peppers")
        )));
        OutputWriter outputWriter = new OutputWriter("src/test/resources");

        outputWriter.write(output);

        Assertions.assertThat(Files.exists(Path.of("src/test/resources/test_output.out.txt"))).isTrue();
    }

    private Ingredient ingredient(String mushrooms) {
        return new Ingredient(mushrooms);
    }

}
