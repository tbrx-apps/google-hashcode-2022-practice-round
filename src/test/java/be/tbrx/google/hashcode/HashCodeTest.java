package be.tbrx.google.hashcode;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import be.tbrx.google.hashcode.input.InputReader;
import be.tbrx.google.hashcode.input.SampleInput;
import be.tbrx.google.hashcode.output.OutputWriter;
import be.tbrx.google.hashcode.output.SampleOutput;
import be.tbrx.google.hashcode.solver.Output;
import be.tbrx.google.hashcode.solver.ProblemSolver;

public class HashCodeTest {

    private InputReader inputReader;
    private OutputWriter outputWriter;

    @BeforeEach
    void setUp() throws IOException {
        inputReader = new InputReader("src/test/resources/input");
        outputWriter = new OutputWriter("src/test/resources/output");
    }

    @Test
    void a_an_example() throws IOException {
        SampleInput sampleInput = inputReader.read("a_an_example.in.txt");

        ProblemSolver problemSolver = new ProblemSolver();
        Output output = problemSolver.solve(sampleInput);

        outputWriter.write(new SampleOutput("a_an_example.out.txt", output));
    }

    @Test
    void b_basic() throws IOException {
        SampleInput sampleInput = inputReader.read("b_basic.in.txt");

        ProblemSolver problemSolver = new ProblemSolver();
        Output output = problemSolver.solve(sampleInput);

        outputWriter.write(new SampleOutput("b_basic.out.txt", output));
    }

    @Test
    void c_coarse() throws IOException {
        SampleInput sampleInput = inputReader.read("c_coarse.in.txt");

        ProblemSolver problemSolver = new ProblemSolver();
        Output output = problemSolver.solve(sampleInput);

        outputWriter.write(new SampleOutput("c_coarse.out.txt", output));
    }

    @Test
    void d_difficult() throws IOException {
        SampleInput sampleInput = inputReader.read("d_difficult.in.txt");

        ProblemSolver problemSolver = new ProblemSolver();
        Output output = problemSolver.solve(sampleInput);

        outputWriter.write(new SampleOutput("d_difficult.out.txt", output));
    }

    @Test
    void e_elaborate() throws IOException {
        SampleInput sampleInput = inputReader.read("e_elaborate.in.txt");

        ProblemSolver problemSolver = new ProblemSolver();
        Output output = problemSolver.solve(sampleInput);

        outputWriter.write(new SampleOutput("e_elaborate.out.txt", output));
    }
}
