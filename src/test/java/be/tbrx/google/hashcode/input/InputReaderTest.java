package be.tbrx.google.hashcode.input;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

public class InputReaderTest {

    @Test
    void readInput() throws IOException {
        InputReader inputReader = new InputReader("src/test/resources");
        SampleInput inputFile = inputReader.read("test_input.in.txt");

        Assertions.assertThat(inputFile).isEqualTo(new SampleInput(3, List.of(
                "2 cheese peppers\n0",
                "1 basil\n1 pineapple",
                "2 mushrooms tomatoes\n1 basil"
        )));
    }

}
