package be.tbrx.google.hashcode.solver;

import be.tbrx.google.hashcode.input.SampleInput;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProblemSolverTest {

    private ProblemSolver problemSolver;

    @BeforeEach
    void setUp() {
        problemSolver = new ProblemSolver();
    }

    @Test
    void oneClient_oneIngredient() {
        Output output = problemSolver.solve(new SampleInput(1, List.of(
                entry("1 cheese", "0")
        )));
        Assertions.assertThat(output).isEqualTo(output("cheese"));
    }

    @Test
    void twoClients_oneIngredient_overLapping() {
        Output output = problemSolver.solve(new SampleInput(2, List.of(
                entry("2 cheese tomato", "0"),
                entry("2 cheese salami", "0")
        )));
        Assertions.assertThat(output).isEqualTo(output("cheese", "tomato", "salami"));
    }

    @Test
    void threeClients_oneIngredient_overLapping_1Dislike() {
        Output output = problemSolver.solve(new SampleInput(3, List.of(
                entry("2 cheese tomato", "0"),
                entry("2 cheese salami", "0"),
                entry("2 cheese pineapple", "1 salami")
        )));
        Assertions.assertThat(output).isEqualTo(output("cheese", "tomato", "pineapple"));
    }

    @Test
    void testAllCombinations(){
        List<List<Ingredient>> allCombinations = problemSolver.getAllCombinations(Set.of(new Ingredient("cheese"),
                new Ingredient("tomato")
        ));

        Assertions.assertThat(allCombinations).containsExactly(
                List.of(new Ingredient("cheese")),
                List.of(new Ingredient("tomato")),
                List.of(new Ingredient("cheese"), new Ingredient("tomato"))
                );
    }

    private Output output(String... ingredients) {
        return new Output(Stream.of(ingredients).map(Ingredient::new).collect(Collectors.toSet()));
    }

    private String entry(String likes, String dislikes) {
        return likes + "\n" +
               dislikes;
    }
}
