package be.tbrx.google.hashcode.solver;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class IngredientCombinatorTest {

    @Test
    void name() {
        Ingredient input[] = {new Ingredient("cheese"), new Ingredient("tomato")};

//        IngredientCombinator.printCombination(input, input.length, 1);

        Assertions.assertThat(IngredientCombinator.printCombination2(input, input.length, 1)).containsExactly(
                new Ingredient("cheese"), new Ingredient("tomato")
        );
    }
}
