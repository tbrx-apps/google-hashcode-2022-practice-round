package be.tbrx.google.hashcode.input;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class InputReader {

    private final String inputPath;

    public InputReader(String inputPath) throws IOException {
        Path path = Paths.get(inputPath);
        if (!Files.exists(path)) {
            Files.createDirectory(path);
        }
        this.inputPath = inputPath;
    }

    public SampleInput read(String resource) throws IOException {
        Path input = Paths.get(inputPath + "/" + resource);
        if (!Files.exists(input)) {
            throw new IOException();
        }

        FileInputStream fileInputStream = new FileInputStream(inputPath + "/" + resource);

        String content = new String(fileInputStream.readAllBytes());
        String[] lines = content.split("\n");

        int totalEntries = getTotalEntries(lines);
        List<String> entries = new ArrayList<>();

        for (int i = 1; i < lines.length; i = i + 2) {
            String firstLine = lines[i];
            String secondLine = lines[i + 1];
            entries.add(firstLine + "\n" + secondLine);
        }

        return new SampleInput(totalEntries, entries);
    }

    private int getTotalEntries(String[] lines) {
        return Integer.parseInt(lines[0]);
    }

}
