package be.tbrx.google.hashcode.input;

import java.util.List;
import java.util.Objects;

public record SampleInput(int totalEntries, List<String> entries) {

    public SampleInput {
        if (totalEntries != entries.size()) {
            throw new RuntimeException("Parsing failed! Check input logic.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SampleInput that = (SampleInput) o;
        return totalEntries == that.totalEntries && Objects.equals(entries, that.entries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(totalEntries, entries);
    }

    @Override
    public String toString() {
        return "SampleInput{" +
               "totalEntries=" + totalEntries +
               ", entries=" + entries +
               '}';
    }
}
