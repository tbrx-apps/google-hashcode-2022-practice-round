package be.tbrx.google.hashcode.output;

import be.tbrx.google.hashcode.solver.Output;

public record SampleOutput(String outputName, Output output) {

    public String getName() {
        return outputName;
    }

    private String getIngredientsAsString() {
        StringBuilder builder = new StringBuilder();
        this.output.ingredients().forEach(ingredient -> builder.append(" ").append(ingredient.name()));
        return builder.toString();
    }

    @Override
    public String toString() {
        String cheese_mushrooms_tomatoes_peppers = getIngredientsAsString();
        return String.format("%s%s", output.total(), cheese_mushrooms_tomatoes_peppers);
    }
}
