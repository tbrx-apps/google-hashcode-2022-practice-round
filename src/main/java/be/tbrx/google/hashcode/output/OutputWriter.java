package be.tbrx.google.hashcode.output;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class OutputWriter {

    private final String outputPath;

    public OutputWriter(String outputPath) throws IOException {
        Path path = Paths.get(outputPath);
        if (!Files.exists(path)) {
            Files.createDirectory(path);
        }
        this.outputPath = outputPath;
    }

    public void write(SampleOutput output) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(String.format(outputPath + "/%s", output.getName()));
        fileOutputStream.write(output.toString().getBytes(StandardCharsets.UTF_8));
        fileOutputStream.close();
    }
}
