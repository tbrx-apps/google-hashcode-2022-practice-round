package be.tbrx.google.hashcode.solver;

import be.tbrx.google.hashcode.input.SampleInput;

import java.util.*;
import java.util.stream.Collectors;

public class ProblemSolver {


    public Output solve2(SampleInput sampleInput) {
        // Get All Clients
        List<Client> clients = sampleInput.entries().stream().map(this::map).toList();

        // Get unique set of Ingredients possible
        Set<Ingredient> ingredients = new HashSet<>();
        clients.forEach(client -> ingredients.addAll(client.getIngredients()));

        List<List<Ingredient>> combinations = getAllCombinations(ingredients);

        return null;
    }

    public List<List<Ingredient>> getAllCombinations(Set<Ingredient> ingredients) {



        return List.of(
                List.of(new Ingredient("cheese")),
                List.of(new Ingredient("tomato")),
                List.of(new Ingredient("cheese"), new Ingredient("tomato"))
        );
    }

    public Output solve(SampleInput sampleInput) {

        // Get All Clients
        List<Client> clients = sampleInput.entries().stream().map(this::map).toList();

        // Get unique set of Ingredients possible
        Set<Ingredient> ingredients = new HashSet<>();
        clients.forEach(client -> ingredients.addAll(client.getIngredients()));

        // Analyse client preferences
        Set<IngredientScoreCard> scoreCards = new HashSet<>();
        ingredients.forEach(ingredient -> {
            IngredientScoreCard ingredientScoreCard = new IngredientScoreCard(ingredient);
            clients.forEach(client -> {
                if (client.likes(ingredient)) ingredientScoreCard.like();
                if (client.dislikes(ingredient)) ingredientScoreCard.dislike();
            });
            scoreCards.add(ingredientScoreCard);
        });

        List<IngredientScoreCard> ingredientsByPopularity = scoreCards.stream().sorted(Comparator.comparing(IngredientScoreCard::total).reversed()).toList();

        //Start processing
        Set<Ingredient> chosenIngredients = new HashSet<>();
        ingredientsByPopularity.stream()
                .filter(ingredientScoreCard -> ingredientScoreCard.total() >= 0)
                .forEach(ingredientScoreCard -> {
                    clients.forEach(client -> {
                        if (client.likes(ingredientScoreCard.getIngredient())) {
                            chosenIngredients.add(ingredientScoreCard.getIngredient());
                        }
                        if (client.dislikes(ingredientScoreCard.getIngredient())) {
                            chosenIngredients.remove(ingredientScoreCard.getIngredient());
                        }
                    });
                });
        return new Output(chosenIngredients);
    }

    private Client map(String entry) {
        String[] entryLines = entry.split("\n");
        List<Ingredient> likes = Arrays.stream(entryLines[0].split(" ")).skip(1).map(Ingredient::new).toList();
        List<Ingredient> disLikes = Arrays.stream(entryLines[1].split(" ")).skip(1).map(Ingredient::new).toList();
        return new Client(likes, disLikes);
    }
}
