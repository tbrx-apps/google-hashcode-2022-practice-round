package be.tbrx.google.hashcode.solver;

import java.util.Objects;

public class IngredientScoreCard {

    private final Ingredient ingredient;
    private int likes;
    private int dislikes;

    public IngredientScoreCard(Ingredient ingredient) {
        this.ingredient = ingredient;
        this.dislikes = 0;
        this.likes = 0;
    }

    public void like() {
        likes++;
    }

    public void dislike() {
        dislikes++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IngredientScoreCard that = (IngredientScoreCard) o;
        return Objects.equals(ingredient, that.ingredient);
    }

    public int total() {
        return likes - dislikes;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ingredient);
    }

    @Override
    public String toString() {
        return "IngredientScoreCard{" +
               "ingredient=" + ingredient +
               ", likes=" + likes +
               ", dislikes=" + dislikes +
               '}';
    }
}
