package be.tbrx.google.hashcode.solver;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Client {

    private final List<Ingredient> likes;
    private final List<Ingredient> dislikes;

    public Client(List<Ingredient> likes, List<Ingredient> dislikes) {
        this.likes = likes;
        this.dislikes = dislikes;
    }


    public List<Ingredient> getLikes() {
        return likes;
    }

    public List<Ingredient> getDislikes() {
        return dislikes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(likes, client.likes) && Objects.equals(dislikes, client.dislikes);
    }

    public boolean likes(Ingredient ingredient) {
        return likes.contains(ingredient);
    }

    public boolean dislikes(Ingredient ingredient) {
        return dislikes.contains(ingredient);
    }

    public Set<Ingredient> getIngredients() {
        return Stream.of(getLikes(), getDislikes()).flatMap(Collection::stream).collect(Collectors.toSet());

    }

    @Override
    public int hashCode() {
        return Objects.hash(likes, dislikes);
    }

    @Override
    public String toString() {
        return "Client{" +
               "likes=" + likes +
               ", dislikes=" + dislikes +
               '}';
    }
}
