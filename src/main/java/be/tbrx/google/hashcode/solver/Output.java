package be.tbrx.google.hashcode.solver;

import java.util.Set;

public record Output(Set<Ingredient> ingredients) {

    public int total(){
        return ingredients.size();
    }

    @Override
    public Set<Ingredient> ingredients() {
        return ingredients;
    }
}