plugins {
    `java-library`
}

repositories {
    mavenCentral()
}

dependencies {

    testImplementation("org.assertj:assertj-core:3.22.0")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.2")

}

tasks {
    test {
        useJUnitPlatform();
    }
}